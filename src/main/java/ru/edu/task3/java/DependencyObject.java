package ru.edu.task3.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly
 */
@Component
public interface DependencyObject {
    String getValue();
}
