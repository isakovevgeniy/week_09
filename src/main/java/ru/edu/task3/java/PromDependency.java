package ru.edu.task3.java;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
//@Qualifier("PROM")
@Profile("PROM")
public class PromDependency implements DependencyObject {
    @Override
    public String getValue() {
        return "PROM";
    }
}
