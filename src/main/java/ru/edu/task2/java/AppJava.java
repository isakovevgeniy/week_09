package ru.edu.task2.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
public class AppJava {

    @Bean
    @Scope("prototype")
    public TimeKeeper timeKeeper() {
        return new TimeKeeper();
    }

    @Bean()
    public Child child() {
        return new Child();
    }

    @Bean
    public MainContainer mainContainer() {
        return new MainContainer(timeKeeper(), child());
    }

    public static MainContainer run(){
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    public static void main(String[] args) {
        run().isValid();
    }
}
