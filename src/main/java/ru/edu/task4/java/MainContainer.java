package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private SomeInterface someInterface;
    @Autowired
    public MainContainer(SomeInterface someInterfaceBean) {
        someInterface = someInterfaceBean;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
